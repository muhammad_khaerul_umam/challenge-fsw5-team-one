import React from "react";
import { BrowserRouter, Route, Switch } from "react-router-dom";
import Home from "./pages/Home";
import Login from "./pages/Login";
import Register from "./pages/Register";
import UserProfile from "./pages/UserProfile";
import UserUpdate from "./pages/UserUpdate";
import Logout from "./pages/Logout";
import Game from "./components/gamestage/MainGame";
import GameList from "./pages/GameList";
import GameDetail from "./pages/GameDetail";
import NotFound from "./pages/NotFound";
import Forgot from "./pages/Forgot";

const Router = () => (
  <BrowserRouter>
    <Switch>
      <Route exact path="/" component={Home} />
      <Route exact path="/login" component={Login} />
      <Route exact path="/userprofile" component={UserProfile} />
      <Route exact path="/userupdate" component={UserUpdate} />
      <Route exact path="/register" component={Register} />
      <Route exact path="/logout" component={Logout} />
      <Route exact path="/game/rock-paper-scissors" component={Game} />
      <Route exact path="/gamelist" component={GameList} />
      <Route exact path="/game-detail/:id" component={GameDetail} />
      <Route exact path="/forgot" component={Forgot} />
      <Route component={NotFound} />
    </Switch>
  </BrowserRouter>
);

export default Router;
