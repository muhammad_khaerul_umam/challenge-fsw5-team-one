import React, { Component } from 'react'
import './App.css';
import { AuthProvider } from './providers/AuthContext'
import Router from './router'

export default class App extends Component {
  
  render() {
    return (
      <AuthProvider>
        <Router/>
      </AuthProvider>
    );
  }
}

