import React, { Component } from 'react'
import firebase from '../services/firebase'
import {firebaseCreateDb} from '../services/firebaseCreateNewDb'

export const AuthContext = React.createContext();

export const AuthProvider = class extends Component {
    state = {
         currentUser : null,
         username: null,
    }
    
    componentDidMount(){
        firebase.auth().onAuthStateChanged(this.onAuthStateChanged)
    }

    onAuthStateChanged = async (user) => {

        if (user){
            let nama = await this.getUsername(user.uid)

            if (!nama)
            {
                console.log('createDB')
                nama = await firebaseCreateDb(user.uid)
            }
            
            this.setState({
                currentUser: user,
                username : await nama
            })
        } else {
            this.setState({
                currentUser: user
            })
        }
    }

    getUsername = async (data) => {
        try {
            const snapshot = await firebase.database().ref(`users/${data}`).once('value')
            const usr = await snapshot.val().username
            return usr
        } catch (error) {
            console.log(error)

            console.log('createDB')
            firebaseCreateDb(this.onAuthStateChanged.uid)
        }
    }

    render() {
        const { children } = this.props
        const contextValue = {
            currentUser : this.state.currentUser,
            username : this.state.username
        }
        return(
            <AuthContext.Provider value={contextValue}>
                { children }
            </AuthContext.Provider>
        )
    }
}


