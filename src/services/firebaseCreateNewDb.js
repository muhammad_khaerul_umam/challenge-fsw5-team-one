import firebase from '../services/firebase'

export const firebaseCreateDb = async (uid) => {
    const data = {
        usename: 'User-' + Date.now(),
        firstname: "",
        lastname: "",
        location: "",
        lastLogin: [Date.now()],
        achievement: "newbie",
        gameHistory: {},
    }

    try {
        // await firebase.database().ref(`users/${uid}`).set({data})
        const uname = 'User-' + Date.now();

        await firebase.database().ref(`users/${uid}`).set({
            username: uname,
            firstname: "",
            lastname: "",
            location: "",
            lastLogin: [Date.now()],
            achievement: "newbie",
            gameHistory: {}
        })
        
        console.log(`new user DB was successfully created \n ${uid}`)
        return uname

    } catch (error) {
        // console.log(error)
        console.log(`error creating new user DB \n ${uid} \n ${error}`)
    }
}