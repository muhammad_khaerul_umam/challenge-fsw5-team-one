import firebase from "firebase";
import "firebase/auth";

// LIVE FIREBASE CHALLENGE
const firebaseConfig = {
  apiKey: "AIzaSyBaotjCYlL7-JaRdnKZnfDZP_lYRQQ7Ltc",
  authDomain: "challenge9-team1.firebaseapp.com",
  databaseURL: "https://challenge9-team1-default-rtdb.firebaseio.com",
  projectId: "challenge9-team1",
  storageBucket: "challenge9-team1.appspot.com",
  messagingSenderId: "833752781618",
  appId: "1:833752781618:web:fd78a8b96e1cd5116a3d2b",
};

// FIREBASE PERSONAL CANDRA
// const firebaseConfig = {
//     apiKey: "AIzaSyCOGjxwLb86p63V5Y_8zgmKOEeNlsmCHsA",
//     authDomain: "chlg9-candra.firebaseapp.com",
//     databaseURL: "https://chlg9-candra-default-rtdb.firebaseio.com",
//     projectId: "chlg9-candra",
//     storageBucket: "chlg9-candra.appspot.com",
//     messagingSenderId: "62760670191",
//     appId: "1:62760670191:web:bdcfcbce822b58489f9c1a"
//   };

export default firebase.initializeApp(firebaseConfig);
