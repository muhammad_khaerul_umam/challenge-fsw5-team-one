import React, { Component, Fragment } from "react";
import firebase from "../services/firebase";
import { AuthContext } from "../providers/AuthContext";
import { Redirect } from "react-router";

//google login
import { signInWithGoogle } from "../services/googleLogin";

// components FE
import Input from "../components/Input";
import NavbarComponent from "../components/NavbarComponent";
import Footer from "../components/Footer";
import classes from "../styles/Login.module.css";
import { Link } from "react-router-dom";

class Login extends Component {
  static contextType = AuthContext;

  state = {
    email: "",
  };

  set = (name) => (event) => {
    // console.log(event.target.value);

    this.setState({
      [name]: event.target.value,
    });
  };

  handleOnSubmit = async (event) => {
    const { email, password } = this.state;
    const { history } = this.props;

    event.preventDefault();

    if (!email || !password) {
      return alert("email & password cant be empty");
    }

    try {
      await firebase.auth().signInWithEmailAndPassword(email, password);
      history.push("/");
      console.log("success");
    } catch (error) {
      alert("failed login");
      console.log(error);
    }
  };

  componentDidMount() {
    document.querySelector(".navbar").style.backgroundColor = "black";
  }

  render() {
    const { currentUser } = this.context;
    if (!!currentUser) {
      return <Redirect to="/" />;
    }

    return (
      <Fragment>
        <NavbarComponent />

        <div className={classes.Login}>
          <h1>Login Page </h1>
          <br />
          <form onSubmit={this.handleOnSubmit}>
            <Input
              labelFor="Email"
              type="email"
              name="email"
              change={this.set("email")}
              value={this.state.email}
            />
            <Input
              labelFor="Password"
              type="password"
              name="password"
              change={this.set("password")}
              value={this.state.password}
            />
            <Input type="submit" value="submit" />
          </form>
          <br></br>
          <span>or </span>
          <button onClick={signInWithGoogle}>Login with Google Account</button>
          <br></br>
          <br></br>
          <p>
            <Link to={"/forgot"} className="text-primary">
              Forgot password?
            </Link>
          </p>
        </div>

        <Footer />
      </Fragment>
    );
  }
}

export default Login;
