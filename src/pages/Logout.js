import React, { Component } from 'react'
import { AuthContext } from '../providers/AuthContext'
import firebase from '../services/firebase'
import { Redirect } from 'react-router'


export default class Logout extends Component {
  static contextType = AuthContext; 

    componentDidMount(event) {
        const { history } = this.props;

        this.logout();
        history.push('/')
    }
  
    logout = event => {
    firebase.auth().signOut().then(() => {
        // Sign-out successful.
        alert('Sign-out successful.')
      }).catch((error) => {
        // An error happened.
        alert('An error happened.')
      });
}

    render() {
        return (
            <>
                
            </>
        )
    }
}
