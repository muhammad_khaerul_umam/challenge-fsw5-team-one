import React, { Component, Fragment } from 'react';

// components FE
import NavbarComponent from '../components/NavbarComponent';
import GameListComponent from '../components/GameListComponent';
import Footer from '../components/Footer';

class Login extends Component {
    

    componentDidMount() {
      document.querySelector(".navbar").style.backgroundColor = "black"
    }


    render() {
      return (
        <Fragment>
          <NavbarComponent/>
          <GameListComponent />
          <Footer/>
        </Fragment>
      )
    }
}

export default Login;