import React, { Component, Fragment } from 'react'
import NavbarComponent from '../components/NavbarComponent'
import UserListComponent from '../components/UserListComponent'
import UserProfileComponent from '../components/UserProfileComponent'
import Footer from '../components/Footer'
import firebase from '../services/firebase'
import { AuthContext } from '../providers/AuthContext'


export default class UserProfile extends Component {
    static contextType = AuthContext; 

    state = {
        userData : {
            username : null,
            firstname : null,
            lastname : null,
            email : null,
            password : null,
            achievement : null,
            avatar : null,
            location : null
        },
        allUserData : null,
        isLoading: true,
        username : null,
        currentUser : null
    }

    componentDidMount() {

        document.querySelector(".navbar").style.backgroundColor = "black";


        // note: need attention, this.context return null after page is refreshed.
        if (this.context) {
            this.getUserData()
        } else {
            console.log('no current user')
        }

        this.getAllUserData()
    }

    getUserData = async () => {
        try {
            const userUid = this.context.currentUser.uid;
            const snapshot = await firebase.database().ref(`users/${userUid}`).once('value')
            const {username, firstname, lastname, achievement, location, lastLogin, email} = snapshot.val();
    
            this.setState({
                userData : {
                    username,
                    firstname,
                    lastname,
                    achievement,
                    email : this.context.currentUser.email,
                    location,
                    lastLogin
                }
        })
        } catch (error) {
            console.log(error)
        }
    }

    getAllUserData = async () => {
        try {
            const snapshot = await firebase.database().ref("users").once('value')
            const allUserData = snapshot.val();
        
            this.setState({
                    isLoading: false,
                    allUserData
            })

        } catch (error) {
            console.log(error)
        }
    }

    get Loader() {
        return <em>Loading...</em>
    }

    render() {
        const { currentUser } = this.context;
        // console.log(currentUser)
        

        return (
            <Fragment>
                <NavbarComponent/>
                <UserProfileComponent 
                    username={this.state.userData.username} 
                    firstname={this.state.userData.firstname} 
                    lastname={this.state.userData.lastname} 
                    location={this.state.userData.location} 
                    achievement={this.state.userData.achievement}
                    email={this.state.userData.email}/>
                {this.state.isLoading ? this.Loader : (<UserListComponent valueFromParent={this.state.allUserData} />)}
                <Footer/>
            </Fragment>
        )
    }
}

