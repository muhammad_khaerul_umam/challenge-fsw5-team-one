import React, { Component, Fragment } from "react";
import firebase from "../services/firebase";
import { Redirect } from "react-router";
import { AuthContext } from "../providers/AuthContext";
// import { Col, Button, Form, FormGroup } from 'react-bootstrap'

//google login
import { signInWithGoogle } from "../services/googleLogin";

// components FE
import Input from "../components/Input";
import NavbarComponent from "../components/NavbarComponent";
import Footer from "../components/Footer";
import classes from "../styles/Login.module.css";

class Register extends Component {
  static contextType = AuthContext;

  state = {};

  set = (name) => (event) => {
    this.setState({
      [name]: event.target.value,
    });
  };

  componentDidMount() {
    document.querySelector(".navbar").style.backgroundColor = "black";
  }

  handleOnChange = (event) => {
    // console.log(event.target);
    this.setState({
      [event.target.name]: event.target.value,
    });
  };

  firebaseCreateDb = async (uid) => {
    const data = {
      usename: "User-" + Date.now(),
      firstname: "",
      lastname: "",
      location: "",
      lastLogin: [Date.now()],
      achievement: "newbie",
      gameHistory: {},
    };

    try {
      // await firebase.database().ref(`users/${uid}`).set({data})
      await firebase
        .database()
        .ref(`users/${uid}`)
        .set({
          username: "User-" + Date.now(),
          firstname: "",
          lastname: "",
          location: "",
          lastLogin: [Date.now()],
          achievement: "newbie",
          gameHistory: {},
        });

      console.log(`new user DB was successfully created`);
    } catch (error) {
      // console.log(error)
      console.log(`error creating new user DB \n ${uid} \n ${error}`);
    }
  };

  handleRegister = (event) => {
    const { email, password } = this.state;
    const { history } = this.props;

    event.preventDefault();
    if (!email || !password) {
      return alert("email & password cant be empty");
    }

    firebase
      .auth()
      .createUserWithEmailAndPassword(email, password)
      .then((userCredential) => {
        // var user = userCredential.user;
        // console.log(userCredential.user.uid);

        this.firebaseCreateDb(userCredential.user.uid);

        // alert(
        //   `An user has been successfully registered \n ${userCredential.user.uid}`
        // );

        history.push("/");
      })
      .catch((error) => {
        // var errorCode = error.code;
        var errorMessage = error.message;

        alert(errorMessage);
      });
  };

  render() {
    const { currentUser } = this.context;

    if (currentUser) return <Redirect to="/" />;
    return (
      <Fragment>
        <NavbarComponent />
        <div className={classes.Login}>
          <h1>Register Page</h1>
          <br />
          <form onSubmit={this.handleRegister}>
            <Input
              labelFor="Email"
              type="email"
              name="email"
              change={this.set("email")}
              value={this.state.email}
            />
            <Input
              labelFor="Password"
              type="password"
              name="password"
              change={this.set("password")}
              value={this.state.password}
            />
            <Input type="submit" value="submit" />
          </form>
          <br></br>
          <button onClick={signInWithGoogle}>Login with Google Account</button>
        </div>
        <Footer />
      </Fragment>
    );
  }
}

export default Register;
