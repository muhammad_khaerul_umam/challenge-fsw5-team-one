import React, { Component } from 'react'
import Footer from '../components/Footer'
import JumbotronComponent from '../components/JumbotronComponent'
import NavbarComponent from '../components/NavbarComponent'
import GameListComponent from '../components/GameListComponent'
import Testimonial from '../components/Testimonial'
import { AuthContext } from '../providers/AuthContext'

export default class Home extends Component {
    static contextType = AuthContext; 

    componentDidMount() {
        window.addEventListener("scroll", this.handleScroll);
    }

    componentWillUnmount() {
        window.removeEventListener("scroll", this.handleScroll);
    }

    handleScroll = () => {
        if (window.scrollY > 200) {
            document.querySelector(".navbar").style.backgroundColor = "black"
        } else {
            document.querySelector(".navbar").style.backgroundColor = "rgba(22, 22, 22, 0.4)"
        }
    }

    getCurrentUser = () => {
        // console.log(this.context.currentUser)
    }

    render() {
        const { currentUser } = this.context;
        return (
            <div>
                <NavbarComponent />
                <JumbotronComponent />
                <GameListComponent />
                <Testimonial />
                <Footer />
            </div>
        )
    }
}
