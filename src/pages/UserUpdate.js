import React, { Component, Fragment } from 'react'
import NavbarComponent from '../components/NavbarComponent'
import UserListComponent from '../components/UserListComponent'
import Input from '../components/InputUpdateForm'
import classes from '../styles/UserProfileComponent.module.css'
import { Container } from 'reactstrap'
import Footer from '../components/Footer'
import firebase from '../services/firebase'
import { AuthContext } from '../providers/AuthContext'

export default class UserProfile extends Component {
    static contextType = AuthContext; 

    state = {
        userData : {
            username : null,
            firstname : null,
            lastname : null,
            email : null,
            password : null,
            achievement : null,
            avatar : null,
            location : null
        },
        allUserData : null,
        isLoading: true,
        username : null,
        currentUser : null
    }

    set = (name) => (event) => {
        // console.log(event.target.value);
    
        this.setState({
          [name]: event.target.value,
        });
      };

    componentDidMount() {

        document.querySelector(".navbar").style.backgroundColor = "black";


        // note: need attention, this.context return null after page is refreshed.
        if (this.context) {
            this.getUserData()
        } else {
            console.log('no current user')
        }

        this.getAllUserData()
    }

    getUserData = async () => {
        try {
            const userUid = this.context.currentUser.uid;
            const snapshot = await firebase.database().ref(`users/${userUid}`).once('value')
            const {username, firstname, lastname, achievement, location, lastLogin, email} = snapshot.val();
    
            this.setState({
                userData : {
                    username,
                    firstname,
                    lastname,
                    achievement,
                    email : this.context.currentUser.email,
                    location,
                    lastLogin
                }
        })
        } catch (error) {
            console.log(error)
        }
    }

    getAllUserData = async () => {
        try {
            const snapshot = await firebase.database().ref("users").once('value')
            const allUserData = snapshot.val();
        
            this.setState({
                    isLoading: false,
                    allUserData
            })

        } catch (error) {
            console.log(error)
        }
    }

    get Loader() {
        return <em>Loading...</em>
    }

    render() {
        const { currentUser } = this.context;
        // console.log(currentUser)
        

        return (
            <Fragment>
                <NavbarComponent/>
                <Container className={classes.container}>
                <h1 className={`${classes.mobileCenter} mt-5`}>Your Profile</h1>
                <div  className="row d-flex">
                    <div className={`${classes.mobileCenter} col-sm-md-lg-6`}>
                        <img className="mx-3" src="https://www.gravatar.com/avatar/205e460b479e2e5b48aec07710c08d50?s=400" />
                    </div>
                    <div className={`${classes.mobileCenter} col-sm-md-lg-6`}>
                    {/* <div className={styles.Login}> */}
                        <form onSubmit={this.handleOnSubmit}>
                        <Input labelFor="Username" type="username" name="username" change={this.set("username")} value={this.state.username}/>
                        <Input labelFor="Firstname" type="firstname" name="firstname" change={this.set("firstname")} value={this.state.firstname}/>
                        <Input labelFor="Lastname" type="lastname" name="lastname" change={this.set("lastname")} value={this.state.lastname}/>
                        <Input labelFor="Email" type="email" name="email" change={this.set("email")} value={this.state.email}/>
                        <Input labelFor="Location" type="location" name="location" change={this.set("location")} value={this.state.location}/>
                        <Input labelFor="Achievement" type="achievement" name="achievement" change={this.set("achievement")} value={this.state.achievement}/>
                        <Input type="submit" value="Submit change" />
                    </form>
                    {/* </div>  */}
                    </div> 
                </div>
                {this.state.isLoading ? this.Loader : (<UserListComponent valueFromParent={this.state.allUserData} />)}
            </Container>
            <Footer/>
            </Fragment>
        )
    }
}
