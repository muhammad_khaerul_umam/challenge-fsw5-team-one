
import {Card,Table} from 'react-bootstrap'

import TableLeaderboard from './TableLeaderboardComponent'
export default function CardGameleaderboad(props) {
    // console.log('Nilai score di game'+props.dataPlayer)
    return (
        <div className="py-5">
            <Card>
            <Card.Header><h4>Leaderboard</h4></Card.Header>
            <Card.Body>
            <Table striped bordered hover size="sm">
                <thead>
                    <tr>
                    <th>Rank</th>
                    <th>Name</th>
                    <th>Score</th>
                    </tr>
                </thead>
                <tbody>
                    {   props.dataPlayer.map((e)=>{
                        return <TableLeaderboard username={e.username} score={e.score} />
                    })
                }
                </tbody>
                </Table>
            </Card.Body>
            </Card>
        </div>
    )
}
