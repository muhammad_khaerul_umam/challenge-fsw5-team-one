import React from 'react'
import {Button} from 'react-bootstrap'


const SectionTitle = () => {
    
    return (
        <div className="text-white">
          <h1 className="font-weight-bold">TOP SCORES</h1>
          <p>This top score from various games provided on this platform</p>
          <Button className="px-4 py-2" variant="danger">See More</Button>
        </div>
    )
}

export default SectionTitle;

