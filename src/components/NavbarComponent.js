// CLASS COMPONENT
import React, { Component } from 'react'
import { Nav, Navbar, NavDropdown } from "react-bootstrap";
import classes from '../styles/Navbar.module.css'
import { AuthContext } from '../providers/AuthContext'
import {Link} from 'react-router-dom'
import firebase from '../services/firebase'


export default class NavbarComponent extends Component {
  static contextType = AuthContext;

  state = {
    username: null
  }

  // componentDidMount = () => {
  //   console.log('1')
  //   console.log(this.context.username)
  //   if (this.context.currentUser) {
  //     this.getUsername()
  //   }
  // }

  // getUsername = async () => {
  //   try {
  //     const userUid = this.context.currentUser.uid;
  //     const snapshot = await firebase.database().ref(`users/${userUid}`).once('value')
      
  //     const { username } = snapshot.val();

  //     this.setState({
  //       username: username
  //     }, () => {console.log("update sini")})

  //   } catch (error) {
  //       console.log(error)
  //   }
  // }

  render() {
    const { currentUser, username } = this.context

    return (
      <Navbar expand="lg" variant="dark" className={classes.navbar} sticky="top">
        <Navbar.Brand className={classes.navbarBrand}><Link to="/">TEAM <strong>O.N.E</strong></Link></Navbar.Brand>
        <Navbar.Toggle aria-controls="basic-navbar-nav" />
        <Navbar.Collapse id="basic-navbar-nav" justify-content-between>
          <Nav className={`${classes.navLink} mr-auto`}>
            <Nav.Link><Link to="/">Home</Link></Nav.Link>
            <Nav.Link><Link to="/gamelist">Games</Link></Nav.Link>
          </Nav>
          
          {currentUser == null && 
            <Nav className={`${classes.navLink} ml-auto`}>
              <Nav.Link><Link to="/register">Register</Link></Nav.Link>
              <Nav.Link><Link to="/login">Login</Link></Nav.Link>
            </Nav>
          }

          {currentUser !== null && 
            <Nav className={`${classes.navLink} ml-auto`}>
              <Nav.Link>{`Hi, ${username}`}</Nav.Link>
              <Nav.Link><Link to="/userprofile">My Profile</Link></Nav.Link>
              <Nav.Link><Link to="/logout">Logout</Link></Nav.Link>
            </Nav> 
          }
  
        </Navbar.Collapse>
      </Navbar>

    );
  }
}

