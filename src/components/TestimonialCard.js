import React from 'react'
import classes from '../styles/TestimonialCard.module.css'


export default function TestimonialCard(props){
    
    return (
        <div className={classes.TestimonialCard}>
            <div className="row">
                <div className="col-3">
                    <img className={classes.avatar} alt=""/>
                </div>
                <p className="text-warning">{props.name}<br/><span className="text-secondary">{props.job}</span></p>
            </div>
            <div className="row">
                <div className="col">
                    <h5 className="text-white">“{props.post}”</h5>
                    <p className="text-secondary">October 18, 2018</p>
                </div>
            </div>
        </div>
    )
}


