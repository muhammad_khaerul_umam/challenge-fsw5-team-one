import React from "react";
import classes from "../styles/GameListCard.module.css"
import { Card, Button } from "react-bootstrap";
import {Link} from 'react-router-dom'
import GameDetail from '../pages/GameDetail'
export const GameListCard = (props) => {
  return (
    <div className={classes.GameListCard}>
      <Card className="align-item-center" style={{ width: "26rem" }} bg="dark">
        <Card.Img variant="top" src={props.img} />
        <Card.Body>
          <Card.Title className="text-white">{props.title}</Card.Title>
          <Card.Text className="text-white">{props.detail}</Card.Text>
          {/* <Link to='/game-detail/' dataName={props.title} dataDetail={props.detail} dataImg={props.img}>
            <Button className="px-4 py-2" variant="danger">Game Details</Button>
          </Link> */}
          <Link to={{ 
          pathname: "/game-detail/"+props.title, 
          state:{
            name: props.title,
            detail: props.detail,
            img:props.img,
            link:props.link
          }
          }}>
            <Button className="px-4 py-2" variant="danger">Game Details</Button>
          </Link>
        </Card.Body>
      </Card>
    </div>
  );
};