// import React, { Component } from 'react'
// import { Container, Table} from 'react-bootstrap'

// export default class Userlist extends Component {
//     state = {
//         allUser : [
//             ["AvatarUser", "Candra", "Candra", "Power Gamer"],
//             ["AvatarUser", "Nisa", "Nisa", "Newbie"],
//             ["AvatarUser", "Aisya", "Aisya", "Newbie"],
//             ["AvatarUser", "Aisya", "Aisya", "Intermediete"]
//         ],
//     }

//     render() {
//         return (
//             <Container>
//             {/* =============== ALL USER PROFILE =====================*/}
//             <h1 className="mt-5">All User Profile</h1>
//                     <Table  hover responsive>
//                         <thead>
//                             <tr>
//                             <th>Avatar</th>
//                             <th>Username</th>
//                             <th>Location</th>
//                             <th>Achievement</th>
//                             <th>Actions</th>
//                             </tr>
//                         </thead>
//                         <tbody>
//                             {
//                                 this.state.allUser.map((numList, i) => (
//                                     <tr>
//                                         {
//                                             numList.map((num, j) => (
//                                                 <td>{num}</td>
//                                             ))
//                                         }
//                                         <td>
//                                             <a href="#" className="mr-2">
//                                                 <button class="btn btn-outline-info">Detail</button>
//                                             </a>
//                                             <a href="#" className="mr-2">
//                                                 <button class="btn btn-outline-info">Add as friend</button>
//                                             </a>
//                                             <a href="#" className="mr-2">
//                                                 <button class="btn btn-outline-info">Chat</button>
//                                             </a>
//                                         </td>
//                                         <br></br>
//                                     </tr>
//                                 ))
//                             }
//                         </tbody>
//                     </Table> 
//             </Container>
//         )
//     }
// }


import React, { Component, Fragment } from 'react'
import { Container, Table} from 'react-bootstrap'

export default class Userlist extends Component {

    state = {
        data : null
    }

    get Content() {
        const data = this.props.valueFromParent
        // this.setState({
        //     data : this.props.valueFromParent
        // })

        const userIds = Object.keys(data)
        // console.log(userIds)

        return userIds.map((el, i) => {
            const detailData = data[el]
            
            return (
                <Fragment key={el}>
                    <tr>
                        <td>{detailData.username}</td>
                        <td>{detailData.location}</td>
                        <td>{detailData.achievement}</td>
                    </tr>
                </Fragment>
            )
            
        })
    }

    render() {
        // const data = this.props.valueFromParent
        // const userIds = Object.keys(data)
        // console.log(userIds)

        // const data_1 = userIds.map((el, i) => {
        //     const detailData = data[el]
        //     console.log(detailData)
        // })


        return (
            <Container>
            {/* =============== ALL USER PROFILE =====================*/}
            <h1 className="mt-5">All User Profile</h1>
                    <Table  hover responsive>
                        <thead>
                            <tr>
                            <th>Username</th>
                            <th>Location</th>
                            <th>Achievement</th>
                            </tr>
                        </thead>
                        <tbody>
                            {
                                this.Content
                            }
                        </tbody>
                    </Table> 
            </Container>
        )
    }
}
