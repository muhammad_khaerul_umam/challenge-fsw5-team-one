import React from 'react'
import {Card,Table} from 'react-bootstrap'
export default function TableLeaderboardComponent(props) {
    return (
        <>
        <tr>
                <th>#</th>
                <th>{props.username}</th>
                <th>{props.score}</th>
            </tr> 
    </>
    )
}
