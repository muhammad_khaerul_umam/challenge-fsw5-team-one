import React, { Component } from "react";
import "./gameBoard.css";
import batuPlayer from "../../assets/images/batu.png";
import kertasPlayer from "../../assets/images/kertas.png";
import guntingPlayer from "../../assets/images/gunting.png";
import batuComp from "../../assets/images/batu.png";
import kertasComp from "../../assets/images/kertas.png";
import guntingComp from "../../assets/images/gunting.png";
import { AuthContext } from "../../providers/AuthContext";
import firebase from "../../services/firebase";
import {Redirect} from 'react-router-dom'

export class gameBoard extends Component {
  static contextType = AuthContext;

  

  state = {
    player: "",
    computer: "",
    winner: "VS",
    classCompPick: "",
    skorPlayer: 0,
    skorComputer: 0,
    onClick: 0,
    gameHistory: null,
    latestScore: null
  };

  componentDidMount() {
    if (this.context.currentUser) {
      this.getUserGameHistoryDB();
    } else {
      console.log("no current user");
      // return history.push('/')
    }
  }

  componentWillUnmount = async () => {
    //update score to database
    

    if(this.context.currentUser){
      const updatedData = [Date.now(),this.state.latestScore]
      const uid = this.context.currentUser.uid
  
      this.setState({
          gameHistory : this.state.gameHistory.push(updatedData)
        }, () => {console.log(this.state.gameHistory)})
  
      try {
        await firebase.database().ref(`users/${uid}/gameHistory`).update(this.state.gameHistory)
        console.log('update DB success')
      } catch (error) {
        console.log(error.message)
      }
    }
    else {
      console.log('good bye')
    }
    
  }
  

  getRandomChoice = () => {
    const items = ["batu", "kertas", "gunting"];
    const item = items[Math.floor(Math.random() * 3)];
    console.log(`komp: ${item}`);
    return item;
  };

  pilihanPlayer = (player) => {
      console.log(`user: ${player}`);
      const computer = this.getRandomChoice();
      let skorPlayer = this.state.skorPlayer;
      let skorComputer = this.state.skorComputer;
      let winner = "";

      //logika game
      if (
        (player === "batu" && computer === "kertas") ||
        (player === "kertas" && computer === "gunting") ||
        (player === "gunting" && computer === "batu")
      ) {
        winner = "Computer Win !";
        skorComputer++;
      } else if (
        (computer === "batu" && player === "kertas") ||
        (computer === "gunting" && player === "batu") ||
        (computer === "kertas" && player === "gunting")
      ) {
        winner = "Player Win ! ";
        skorPlayer++;
      } else {
        winner = "Draw !";
      }

      //Update data ke state
      this.setState((prevState) => {
          return {
            ...prevState,
            player,
            computer,
            winner,
            skorComputer,
            skorPlayer,
            onClick: prevState.onClick + 1
          }
      }, () => {
        console.log(this.state.onClick)
        if(this.state.onClick == 3){
          let newScore = this.state.latestScore

          console.log('permainan selesai')
          if(this.state.skorPlayer > this.state.skorComputer){
            newScore += 10
            console.log('player menang, point +10')
          }
          else if (this.state.skorPlayer == this.state.skorComputer)
            console.log('draw')
          else {
            newScore -= 5
            console.log('player kalah, point -5')  
          }

          this.setState({
            latestScore: newScore,
            onClick : 0,
            skorComputer : 0,
            skorPlayer : 0
          })
        }
          
      })

      
  }

  getUserGameHistoryDB = async () => {
    try {
        const userUid = this.context.currentUser.uid;
        const snapshot = await firebase
          .database()
          .ref(`users/${userUid}`)
          .once("value");

        let { gameHistory } = snapshot.val();
        let score = snapshot.val().gameHistory;
        let latestScore = 0;

        if (typeof score !== 'undefined'){
          latestScore = gameHistory[gameHistory.length-1][1]
          // console.log(gameHistory)
        } else {
          gameHistory = [Date.now(), 0]
        }

        // console.log(`lastest score : ${latestScore}`)
        // console.log(gameHistory)

        this.setState({
          latestScore,
          gameHistory
        });

    } catch (error) {
      console.log(error);
    }
  };

  render() {

    const { currentUser } = this.context;

    if (currentUser) {
      console.log('welcome')
    }
    else{
      alert('Please login in order to play this game')
      return <Redirect to="/login" />;
    }

    const { player } = this.state;
    const { computer } = this.state;

    return (
      <div className="main__battle">
        <div className="score__board">
          <h2>Your Score : {this.state.latestScore}</h2>
        </div>
        <div className="score__board">
          <div id="user-label" className="player__score">
            user
          </div>
          <div id="computer-label" className="player__score">
            comp
          </div>
          <span id="user-score">{this.state.skorPlayer}</span> :{" "}
          <span id="computer-score">{this.state.skorComputer}</span>
        </div>

        <div id="result" className="result__battle">
          <p>{this.state.winner}</p>
        </div>

        <div className="choices">
          <div className="choices__player">
            <button
              id="batu_p"
              className={`batu__player ${player === "batu" ? "highlight" : ""}`}
              onClick={() => this.pilihanPlayer("batu")}
            >
              <img src={batuPlayer} alt="batuplayer" />
            </button>
            <button
              id="kertas_p"
              className={`kertas__player ${
                player === "kertas" ? "highlight" : ""
              }`}
              onClick={() => this.pilihanPlayer("kertas")}
            >
              <img src={kertasPlayer} alt="batuplayer" />
            </button>
            <button
              id="gunting_p"
              className={`gunting__player ${
                player === "gunting" ? "highlight" : ""
              }`}
              onClick={() => this.pilihanPlayer("gunting")}
            >
              <img src={guntingPlayer} alt="batuplayer" />
            </button>
          </div>
          <div className="choices__comp">
            <div
              id="batu-c"
              className={`batu__comp ${computer === "batu" ? "highlight" : ""}`}
            >
              <img src={batuComp} alt="batuplayer" />
            </div>
            <div
              id="kertas-c"
              className={`kertas__comp ${
                computer === "kertas" ? "highlight" : ""
              }`}
            >
              <img src={kertasComp} alt="batuplayer" />
            </div>
            <div
              id="gunting-c"
              className={`gunting__comp ${
                computer === "gunting" ? "highlight" : ""
              }`}
            >
              <img src={guntingComp} alt="batuplayer" />
            </div>
          </div>
        </div>
      </div>
      
    );
  }
}

export default gameBoard;
