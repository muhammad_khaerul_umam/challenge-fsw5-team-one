import React, { Component } from "react";
import "./Main.css";
import HeaderGame from "./HeaderGame";
import GameBoard from "./GameBoard";
import ButtonControl from "./ButtonControl";


export default class MainGame extends Component {
  render() {
    return (
      <div className="main__game">
        <div className="game__overlay">
          <HeaderGame />
          <GameBoard />
          <ButtonControl />
        </div>
      </div>
    );
  }
}
