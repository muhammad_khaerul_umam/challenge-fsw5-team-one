import React from "react";
import { Jumbotron, Button, Container }from "react-bootstrap";
import classes from '../styles/Jumbotron.module.css'
import {Link} from 'react-router-dom'

const JumbotronComponent = () => {
  return (
    <div className="text-center">
      <Jumbotron fluid className={classes.jumbotronCustom}>
        <Container>
          <h1 className={`py-2 my-2 ${classes.title}`}>PLAY TRADITIONAL GAME</h1>
          <p className={`py-2 my-3 ${classes.lead}`}>Experience New Traditional Game</p>
          <p>
            <Button className="px-4 py-2" variant="danger"><Link to="/gamelist">PLAY NOW</Link></Button>
          </p>
        </Container>
      </Jumbotron>
    </div>
  );
};

export default JumbotronComponent;
