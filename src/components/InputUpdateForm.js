import React from 'react'

export default function InputUpdateForm(props) {
    return (
        <div>
            <label>
                <strong>{props.labelFor}</strong>
            </label>
            <br />
            <input 
                type        = {props.type} 
                value       = {props.value} 
                onChange    = {props.change} 
                name        = {props.name} 
                placeholder = {`${props.value}`}>
            </input>
        </div>
    )
}