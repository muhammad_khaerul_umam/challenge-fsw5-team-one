// import React, { Component } from 'react'
// import { Container, Table } from 'reactstrap'
// import classes from '../styles/UserProfileComponent.module.css'

// export default class Userprofilecomponent extends Component {
//     state = {
//         userData : {
//             username : "candra",
//             firstname : "candra",
//             lastname : "herdianto",
//             email : "ch@mail.com",
//             password : "123456",
//             achievement : "newbie",
//             avatar : "blank",
//             location : "Yogyakarta"
//         }
//     }

//     render() {
//         return (
//             <Container className={classes.container}>
//                 <h1 className={`${classes.mobileCenter} mt-5`}>Your Profile</h1>
//                 <div  className="row d-flex">
//                     <div className={`${classes.mobileCenter} col-sm-md-lg-6`}>
//                         <img className="mx-3" src="https://www.gravatar.com/avatar/205e460b479e2e5b48aec07710c08d50?s=400" />
//                     </div>
//                     <div className={`${classes.mobileCenter} col-sm-md-lg-6`}>
//                         <Table responsive borderless className="px-3">
//                             <tbody>
//                                     <tr>
//                                         <td>Username</td>
//                                         <td>{this.state.userData.username}</td>
//                                     </tr>   
//                                     <tr>
//                                         <td>Firstname</td>
//                                         <td>{this.state.userData.firstname}</td>
//                                     </tr>
//                                     <tr>
//                                         <td>Lastname</td>
//                                         <td>{this.state.userData.lastname}</td>
//                                     </tr>
//                                     <tr>
//                                         <td>Password</td>
//                                         <td>{this.state.userData.password}</td>
//                                     </tr>
//                                     <tr>
//                                         <td>Email</td>
//                                         <td>{this.state.userData.email}</td>
//                                     </tr>
//                                     <tr>
//                                         <td>Location</td>
//                                         <td>{this.state.userData.location}</td>
//                                     </tr>
//                                     <tr>
//                                         <td>Achievement</td>
//                                         <td>{this.state.userData.achievement}</td>
//                                     </tr>
//                                     <tr>
//                                         <td>
//                                             <a href="#">
//                                                 <button class="btn btn-outline-info">Update</button>
//                                             </a>
//                                         </td>
//                                     </tr>
                                
//                             </tbody>    
//                         </Table> 
//                     </div> 
//                 </div>
//             </Container>
//         )
//     }
// }


import React, { Component } from 'react'
import { Container, Table } from 'reactstrap'
import classes from '../styles/UserProfileComponent.module.css'
import { Link } from 'react-router-dom';

export default function Userprofilecomponent(props) {
    return (
        <Container className={classes.container}>
            <h1 className={`${classes.mobileCenter} mt-5`}>Your Profile</h1>
            <div  className="row d-flex">
                <div className={`${classes.mobileCenter} col-sm-md-lg-6`}>
                    <img className="mx-3" src="https://www.gravatar.com/avatar/205e460b479e2e5b48aec07710c08d50?s=400" />
                </div>
                <div className={`${classes.mobileCenter} col-sm-md-lg-6`}>
                    <Table responsive borderless className="px-3">
                        <tbody>
                                <tr>
                                    <td>Username</td>
                                    <td>{props.username}</td>
                                </tr>   
                                <tr>
                                    <td>Firstname</td>
                                    <td>{props.firstname}</td>
                                </tr>
                                <tr>
                                    <td>Lastname</td>
                                    <td>{props.lastname}</td>
                                </tr>
                                <tr>
                                    <td>Email</td>
                                    <td>{props.email}</td>
                                </tr>
                                <tr>
                                    <td>Location</td>
                                    <td>{props.location}</td>
                                </tr>
                                <tr>
                                    <td>Achievement</td>
                                    <td>{props.achievement}</td>
                                </tr>
                                <tr>
                                    <td>
                                        <Link
                                            to="/userupdate"
                                            className=""
                                            onClick=""
                                        >

                                            <button class="btn btn-outline-info">Update</button>
                                        </Link>
                                    </td>
                                </tr>
                            
                        </tbody>    
                    </Table> 
                </div> 
            </div>
        </Container>
    )
}
